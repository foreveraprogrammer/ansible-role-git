require 'serverspec'
set :backend, :exec


describe package('git'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end


describe file('.gitignore') do
  it { should exist }
  it { should be_file }
  it { should contain '.kitchen/*' }
end
